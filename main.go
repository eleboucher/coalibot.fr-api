package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/genesixx/coalibot.fr-api/FortyTwo"
)

func main() {
	r := mux.NewRouter()
	// Routes consist of a path and a handler function.
	r.HandleFunc("/42/leaderboard", FortyTwo.Leaderboard)
	r.HandleFunc("/42/login", FortyTwo.Login).Methods("POST")
	r.HandleFunc("/42/refreshToken", FortyTwo.RefreshToken).Methods("POST")
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})
	// Bind to a port and pass our router in
	loggedRouter := handlers.CombinedLoggingHandler(os.Stdout, c.Handler(r))

	log.Fatal(http.ListenAndServe(":8000", loggedRouter))
}
